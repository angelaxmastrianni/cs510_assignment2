# README - CS510 HW 2 #

Angela Mastrianni

### File Contents ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Evaluation Documentation ###

#### Basic Classifier ####

For my basic classifer, I created a Naive Bayes classifier which used unigrams as features, removing all punctuation. My classifier summed
the logs of all the probabilities and used add one smoothing, as defined [here](https://web.stanford.edu/~jurafsky/slp3/4.pdf). 
I also set it up to classify a review as neutral if the difference between the negative and positive probability was less then one. 

I split the movie reviews data into 80% for training and 20% for testing. I trained the classifier twice - once on just the movie reviews training 
data and once on the all reviews data. 

I used [multiclass metics](https://towardsdatascience.com/multi-class-metrics-made-simple-part-i-precision-and-recall-9250280bddc2) to calculate
the precision, recall and F1 score for the positive and negative class. I didn't calculate any metrics for the neutral class since there were
no true neutrals in the testing data. 

These were the results for the basic classifier:

Measurment               | Trained On Movie Reviews |  Trained On All Reviews 
-------------            | ------------- | -------- 
Positive Class Precision | 0.90          | 0.92
Positive Class Recall    | 0.96          | 0.97
Positive Class F1 Score  | 0.93          | 0.95
Negative Class Precision | 0.84          | 0.91 
Negative Class Recall    | 0.36          | 0.44
Negative Class F1 Score  | 0.51          | 0.59
Macro F1 Score           | 0.72          | 0.76
Weighted F1 Score        | 0.84          | 0.87

Three example sentences where a review was incorrectly classified with the basic classifier:

1. Input: "Not great movie. Would not recommend" Output: Positive   
Reasoning: The negation makes this sentence hard to classify. 
The classifier is not sophisticated enough to correctly handle the word not in front of great and recommend.
	
2. Input: "Fantastic and wonderful book but bad movie" Output: Positive   
Reasoning: The classifier does not understand that the positive words
belong to book and the negative word belongs to movie. Since there are more positive words, the classifier assigns it a positive label.

3. Input: "A lot of people said this movie was awful and terrible. I couldn't disagree more" Output: Negative   
Reasoning: This text is hard to classify because the author is describing how other people felt and then saying that they disagree. 
The classifier is not nuanced enough to understand that the author is disagreeing with the negative words.

#### Best Classifier ####

To improve the basic classifier, I made the following improvments:

1. Added bigrams to represent negation.  
In the paper ["An Improved Text Sentiment Classification Model Using TF-IDF and Next Word Negation"](https://arxiv.org/pdf/1806.06407.pdf), 
the authors showed that using a biagram to represent negation improved sentiment classification. I did something similar in my classifer.
Whenever there is a word with "n't" or the words no, never or not, a bigram is created with a negation plus the very next word in the sentence.

2. Ignored stop words.
The best classifier ignores the top ten most frequent words in both the positive and negative dictionaries as these are usually stop words
that do not provide any additional meaning.

3. Lowercase all words.
Case is generally not indicative of positive or negative sentiment. 

The best classifier had the following results:

Measurment               | Trained On Movie Reviews | Trained On All Reviews 
-------------            | ------------- | -------- 
Positive Class Precision | 0.95          | 0.95
Positive Class Recall    | 0.94          | 0.96
Positive Class F1 Score  | 0.95          | 0.96
Negative Class Precision | 0.87          | 0.90 
Negative Class Recall    | 0.61          | 0.55
Negative Class F1 Score  | 0.72          | 0.68
Macro F1 Score           | 0.83          | 0.82
Weighted F1 Score        | 0.90          | 0.90

The text "Not great movie. Would not recommend" failed in the basic classifier but was correctly classified in the best classifier 
due to the bigram negations. However, the texts "Fantastic and wonderful book but bad movie" and "A lot of people said this movie was awful and terrible.
I couldn't disagree more" were incorrectly classified in both classifiers so there is still room of improvement in the best classifier.
