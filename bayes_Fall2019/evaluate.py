from __future__ import print_function, division
import os
import sys

from bayesBest import Best_Bayes_Classifier
from bayes_template import Bayes_Classifier

def getClassifier():
	if len(sys.argv) > 2 and sys.argv[2] == "Best":
		return Best_Bayes_Classifier(trainDir)
	else:
		return Bayes_Classifier(trainDir)

def getTestDirectory():
	if len(sys.argv) > 1 and sys.argv[1] is not None:
		return sys.argv[1]
	else:
		return "testing/"

testFile = "bayes_template.py"
trainDir = "movies_reviews/"
testDir = "testing/"

execfile(testFile)
bc = getClassifier()
bc.train()


iFileList = []

for fFileObj in os.walk(testDir + "/"):
	iFileList = fFileObj[2]
	break

print('%d test reviews.' % len(iFileList))

results = {"negative":0, "neutral":0, "positive":0}

true_positives = 0.0
all_predicted_positives = 0.0
all_actual_positives = 0.0

true_negatives = 0.0
all_predicted_negatives = 0.0
all_actual_negatives = 0.0

print("\nFile Classifications:")
for filename in iFileList:
	fileText = bc.loadFile(testDir + filename)
	result = bc.classify(fileText)
	sentiment = int(filename.split("-")[1])

	if result == "positive":
		all_predicted_positives = all_predicted_positives + 1
	elif result == "negative":
		all_predicted_negatives = all_predicted_negatives + 1

	if sentiment == 5:
		all_actual_positives = all_actual_positives + 1
	else:
		all_actual_negatives = all_actual_negatives + 1

	if sentiment == 5 and result == "positive":
		true_positives = true_positives + 1
	elif sentiment == 1 and result == "negative":
		true_negatives = true_negatives + 1


	print ("%s: %s" % (filename, result))
	results[result] += 1

print('\nResults Summary:')
for r in results:
	print("%s: %d" % (r, results[r]))

positive_precision = true_positives / all_predicted_positives
negative_precision = true_negatives / all_predicted_negatives

positive_recall = true_positives / all_actual_positives
negative_recall = true_negatives / all_actual_negatives

positive_f1_score = (2.0*(positive_precision*positive_recall)) / (positive_precision + positive_recall)
negative_f1_score = (2.0*(negative_precision*negative_recall)) / (negative_precision + negative_recall)

macro_f1_score = (positive_f1_score+negative_f1_score) / 2.0
weighted_f1_score = ((positive_f1_score*all_actual_positives) + (negative_f1_score* all_actual_negatives)) / (all_actual_positives + all_actual_negatives)

print('Positive Class Precision: %f \n' % positive_precision)
print('Positive Class Recall: %f \n' % positive_recall)
print('Positive Class F1 Score: %f \n' % positive_f1_score)

print('Negative Class Precision: %f \n' % negative_precision)
print('Negative Class Recall: %f \n' % negative_recall)
print('Negative Class F1 Score: %f \n' % negative_f1_score)

print('Macro F1 Score: %f \n' % macro_f1_score)
print('Weighted F1 Score: %f \n' % weighted_f1_score)

