import math

alphabet_regex = '[a-zA-Z]'

def determine_sentiment(negative_probability, positive_probability):
    """
    :param negative_probability: probability that the word is negative
    :param positive_probability: probability that the word is positive
    :return: if word is positive, negative or neutral
    """
    difference = negative_probability - positive_probability
    if (difference > -1.0 and difference < 1.0):
        return "neutral"
    elif (negative_probability < positive_probability):
        return "positive"
    else:
        return "negative"

def calculate_word_probability(word, chosen_dict, total_num_words, num_distinct_words):
    """
    :param word: word that we are calculating the probability for
    :param chosen_dict: negative or positive dictionary
    :param total_num_words: the total number of words
    :param num_distinct_words: total number of distinct words
    :return: probability that the word is negative or positive depending on the provided dictionary
    """
    freq = 0
    if word in chosen_dict:
        freq = chosen_dict[word]
    probability = (freq + 1.0) / (total_num_words + num_distinct_words)
    return math.log(probability)