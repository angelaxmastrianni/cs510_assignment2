import os
import random

lFileList = []
for fFileObj in os.walk("movies_reviews/"):
    lFileList = fFileObj[2]

testing_number = int(len(lFileList) * 0.2)
testing_files = random.sample(lFileList, testing_number)


for testing_file in testing_files:
    os.rename("movies_reviews/" + testing_file, "testing/" + testing_file)