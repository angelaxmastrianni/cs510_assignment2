from __future__ import division

import operator
import os
import pickle
import re

from bayes_helper_functions import *


class Best_Bayes_Classifier:

    def __init__(self, trainDirectory="movie_reviews/"):
        '''This method initializes and trains the Naive Bayes Sentiment Classifier.  If a
        cache of a trained classifier has been stored, it loads this cache.  Otherwise,
        the system will proceed through training.  After running this method, the classifier
        is ready to classify input text.'''
        self.pos_docs_count = 0
        self.neg_docs_count = 0
        self.train_directory = trainDirectory

    def train(self):
        lFileList = []
        for fFileObj in os.walk(self.train_directory):
            lFileList = fFileObj[2]

        pos_dict = {}
        neg_dict = {}
        for fileName in lFileList:
            sentiment = int(fileName.split("-")[1])
            loaded_file = self.loadFile(self.train_directory + fileName)
            tokens = self.tokenize(loaded_file)
            if sentiment == 1:
                self.neg_docs_count = self.neg_docs_count + 1
                addWordsToDict(neg_dict, tokens)
            elif sentiment == 5:
                self.pos_docs_count = self.pos_docs_count + 1
                addWordsToDict(pos_dict, tokens)
        self.save(pos_dict, "positive_dictionary")
        self.save(neg_dict, "negative_dictionary")

    def classify(self, sText):
        '''Given a target string sText, this function returns the most likely document
        class to which the target string belongs. This function should return one of three
        strings: "positive", "negative" or "neutral".
        '''
        tokens = self.tokenize(sText)

        pos_dict = self.load("positive_dictionary")
        neg_dict = self.load("negative_dictionary")

        sorted_pos_dict = sorted(pos_dict.items(), key=operator.itemgetter(1))
        sorted_neg_dict = sorted(pos_dict.items(), key=operator.itemgetter(1))
        sorted_neg_dict_keys = []
        stop_words = []

        for word_freq in sorted_neg_dict[len(sorted_neg_dict) - 10:]:
            sorted_neg_dict_keys.append(word_freq[0])

        for word_freq in sorted_pos_dict[len(sorted_pos_dict) - 10:]:
            if word_freq[0] in sorted_neg_dict_keys:
                stop_words.append(word_freq[0])

        positive_conditional_probability = calculateProbability(pos_dict, tokens, neg_dict, stop_words)
        positive_prior_probability = self.pos_docs_count / (self.pos_docs_count + self.neg_docs_count)
        positive_probability = positive_conditional_probability + math.log(positive_prior_probability)

        negative_conditional_probability = calculateProbability(neg_dict, tokens, pos_dict, stop_words)
        negative_prior_probability = self.neg_docs_count / (self.pos_docs_count + self.neg_docs_count)
        negative_probability = negative_conditional_probability + math.log(negative_prior_probability)

        return determine_sentiment(negative_probability, positive_probability)

    def loadFile(self, sFilename):
        '''Given a file name, return the contents of the file as a string.'''

        f = open(sFilename, "r")
        sTxt = f.read()
        f.close()
        return sTxt

    def save(self, dObj, sFilename):
        '''Given an object and a file name, write the object to the file using pickle.'''

        f = open(sFilename, "w")
        p = pickle.Pickler(f)
        p.dump(dObj)
        f.close()

    def load(self, sFilename):
        '''Given a file name, load and return the object stored in the file.'''

        f = open(sFilename, "r")
        u = pickle.Unpickler(f)
        dObj = u.load()
        f.close()
        return dObj

    def tokenize(self, sText):
        '''Given a string of text sText, returns a list of the individual tokens that
        occur in that string (in order).'''

        lTokens = []
        sToken = ""
        for c in sText:
            if re.match("[a-zA-Z0-9]", str(c)) != None or c == "\'" or c == "_" or c == '-':
                sToken += c
            else:
                if sToken != "":
                    lTokens.append(sToken)
                    sToken = ""
                if c.strip() != "":
                    lTokens.append(str(c.strip()))

        if sToken != "":
            lTokens.append(sToken)

        return lTokens


def addWordsToDict(chosen_dict, words):
    negate = False
    for word in words:
        word = word.lower()
        if "n't" in word or word == "not" or word == "no" or word == "never":
            negate = True
        else:
            if negate:
                word = "not_" + word
                negate = False
            if word in chosen_dict:
                freq = chosen_dict[word]
                new_freq = freq + 1
                chosen_dict[word] = new_freq
            elif re.search(alphabet_regex, word):
                chosen_dict[word] = 1


def calculateProbability(chosen_dict, words, other_dict, stop_words):
    total_probability = 0
    total_num_words = 0
    for value in chosen_dict.values():
        total_num_words = total_num_words + value
    num_distinct_words = len(chosen_dict.keys()) + len(other_dict.keys())
    negate = False
    for word in words:
        word = word.lower()
        if "n't" in word or word == "not" or word == "no" or word == "never":
            negate = True
        else:
            if negate:
                word = "not_" + word
                negate = False
            if re.search(alphabet_regex, word) and word not in stop_words:
                total_probability = total_probability + calculate_word_probability(word, chosen_dict,
                                                                                       total_num_words,
                                                                                       num_distinct_words)

    return total_probability
